---
title: Blogging
seo_title: Blogging
summary: What am I going to talk about?
seo_desc: Ronin post in a series of test post - SEO Version
date: 2020-08-13T03:17:57.500Z
---

### Talking about a bunch of stuff

Blogging is plenty of talking

God creates dinosaurs. God destroys dinosaurs. God creates Man. Man destroys God. Man creates Dinosaurs. Do you have any idea how long it takes those cups to decompose. I was part of something special. Forget the fat lady! You're obsessed with the fat lady! Drive us out of here!

Eventually, you do plan to have dinosaurs on your dinosaur tour, right? Just my luck, no ice. Checkmate... Yes, Yes, without the oops! Is this my espresso machine? Wh-what is-h-how did you get my espresso machine? This thing comes fully loaded. AM/FM radio, reclining bucket seats, and... power windows.

    Did he just throw my cat out of the window?
    Hey, you know how I'm, like, always trying to save the planet?
    Here's my chance.
    Yeah, but your scientists were so preoccupied with whether or not they could, they didn't stop to think if they should.
    God creates dinosaurs.
    God destroys dinosaurs.
    God creates Man.
    Man destroys God.
    Man creates Dinosaurs.
