---
title: Weight Loss
seo_title: Weight Loss
summary: Common misconceptions and pitfalls
seo_desc: Ronin post in a series of test posts - SEO Version
date: 2020-08-15T03:17:57.500Z
---

### Weight Loss

A bunch of people want to lose weight but get it wrong.

God creates dinosaurs. God destroys dinosaurs. God creates Man. Man destroys God. Man creates Dinosaurs. Do you have any idea how long it takes those cups to decompose. I was part of something special. Forget the fat lady! You're obsessed with the fat lady! Drive us out of here!

Eventually, you do plan to have dinosaurs on your dinosaur tour, right? Just my luck, no ice. Checkmate... Yes, Yes, without the oops! Is this my espresso machine? Wh-what is-h-how did you get my espresso machine? This thing comes fully loaded. AM/FM radio, reclining bucket seats, and... power windows.

    Did he just throw my cat out of the window?
    Hey, you know how I'm, like, always trying to save the planet?
    Here's my chance.
    Yeah, but your scientists were so preoccupied with whether or not they could, they didn't stop to think if they should.
    God creates dinosaurs.
    God destroys dinosaurs.
    God creates Man.
    Man destroys God.
    Man creates Dinosaurs.
